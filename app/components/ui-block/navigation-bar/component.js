import Component from '@ember/component';

const items = [
  { title: 'Index', route: 'index' },
  { title: 'About', route: 'about' }
];

export default Component.extend({
  classNames: [ 'ui-block-navigation-bar' ],

  applicationName: 'Quotes',
  items

});
